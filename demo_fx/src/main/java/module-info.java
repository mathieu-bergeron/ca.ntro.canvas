module ca.ntro.demo.fx {

    requires ca.ntro.core;

    requires ca.ntro.jdk;
    requires ca.ntro.fx;

    requires ca.ntro.demo.core;

    requires javafx.base;
    requires javafx.controls;
	requires javafx.fxml;
	requires javafx.graphics;
	requires javafx.media;
	
	exports ca.ntro.demo.fx;
	
	opens ca.ntro.demo.fx to javafx.fxml;

}
