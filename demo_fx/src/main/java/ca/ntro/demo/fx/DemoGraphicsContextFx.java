package ca.ntro.demo.fx;

import ca.ntro.demo.core.DemoGraphicsContext;
import ca.ntro.fx.NtroGraphicsContextFx;
import javafx.scene.paint.Color;

public class DemoGraphicsContextFx extends NtroGraphicsContextFx implements DemoGraphicsContext {
	
	@Override
	public void setFill(String colorName) {
		getGcFx().setFill(Color.web(colorName));
	}

}
