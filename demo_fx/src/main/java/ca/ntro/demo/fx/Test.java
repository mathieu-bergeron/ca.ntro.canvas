package ca.ntro.demo.fx;

import ca.ntro.core.example.Task;
import ca.ntro.core.example.TaskCore;
import ca.ntro.fx.NtroFx;

public class Test {
	
	static {
		
		NtroFx.initialize();
		
	}
	
	public static void main(String[] args) {
		
		// XXX: not «illegal» to call new
		//      but this is not recommanded
		//      should call factory
		TaskCore tc = new TaskCore();
		
		Task t = Task.newInstance();

		t.isPreconditionOf(Task.newInstance());

		System.out.println(t.getClass());
	}

}
