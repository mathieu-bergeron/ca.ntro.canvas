package ca.ntro.demo.fx;

import java.net.URL;
import java.util.ResourceBundle;

import ca.ntro.core.NtroCanvas;
import ca.ntro.demo.core.DemoGraphicsContext;
import ca.ntro.demo.core.RootView;
import ca.ntro.fx.NtroCanvasFx;
import ca.ntro.fx.ViewFx;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;

public class RootViewFx extends ViewFx implements RootView {
	
	@FXML
	private Canvas canvas;

	private NtroCanvas<DemoGraphicsContext> ntroCanvas;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		ntroCanvas = NtroCanvasFx.newInstance(canvas);
	}

	@Override
	public NtroCanvas<DemoGraphicsContext> canvas() {
		return ntroCanvas;
	}


}
