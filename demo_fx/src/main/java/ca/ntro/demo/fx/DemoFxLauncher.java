package ca.ntro.demo.fx;

import java.io.IOException;
import java.net.URL;

import ca.ntro.core.Ntro;
import ca.ntro.core.NtroGraphicsContext;
import ca.ntro.demo.core.Demo;
import ca.ntro.demo.core.RootView;
import ca.ntro.fx.NtroFx;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class DemoFxLauncher extends Application {
	
	static {

		NtroFx.initialize();

		Demo.initialize();
		
		Ntro.factory().registerSpecialization(NtroGraphicsContext.class, DemoGraphicsContextFx.class);

	}

	
	public static void main(String[] args) {
		Application.launch(args);
	}

    private static URL urlFromPath(String path) {
        URL url = DemoFxLauncher.class.getResource(path);

        if(url == null) {
            throw new RuntimeException("Not found " + path);
        }

        return url;
    }


	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader loader = new FXMLLoader(urlFromPath("/root.xml"));

		try {

			Parent parent = loader.load();
			RootView rootView = loader.getController();
			
			URL cssUrl = urlFromPath("/prod.css");
			parent.getStylesheets().add(cssUrl.toExternalForm());
			
			primaryStage.setWidth(640);
			primaryStage.setHeight(320);
			primaryStage.setScene(new Scene(parent, 640, 320));
			primaryStage.show();
			
			Demo demo = new Demo();
			demo.runDemo(rootView);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
