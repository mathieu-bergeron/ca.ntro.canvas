package ca.ntro;

public interface Logger {

    void log(String message);
}
