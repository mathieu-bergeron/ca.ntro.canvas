package ca.ntro.example_api;

import ca.ntro.Ntro;

public interface Task {
	
	public static Task newInstance() {
		Task instance = Ntro.factory().newInstance(Task.class);
		return instance;
	}

	void isPreconditionOf(Task otherTask);
}
