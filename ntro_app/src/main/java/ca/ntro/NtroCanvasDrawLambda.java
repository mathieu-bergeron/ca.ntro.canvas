package ca.ntro;

public interface NtroCanvasDrawLambda<GC extends NtroGraphicsContext> {
	
	void draw(GC gc);

}
