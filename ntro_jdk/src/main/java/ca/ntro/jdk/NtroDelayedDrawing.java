package ca.ntro.jdk;

import ca.ntro.core.NtroCanvasDrawLambda;
import ca.ntro.core.NtroGraphicsContext;

public class NtroDelayedDrawing {

	private NtroCanvasDrawLambda delayedLambda = null;

	public synchronized void drawNow(NtroGraphicsContext gc) {
		if(delayedLambda != null) {
			delayedLambda.draw(gc);
		}
	}

	public synchronized void drawWhenReady(NtroCanvasDrawLambda lambda) {
		delayedLambda = lambda;
	}
}
