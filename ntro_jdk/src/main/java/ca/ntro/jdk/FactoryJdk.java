package ca.ntro.jdk;

import java.lang.reflect.InvocationTargetException;

import ca.ntro.core.FactoryCore;

public class FactoryJdk extends FactoryCore {


	protected <T> T buildNewInstance(Class<? extends T> instanceType) {
		T instance = null;

		try {

			instance = instanceType.getConstructor().newInstance();

		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			
			e.printStackTrace();
			throw new RuntimeException("[FATAL] cannot instantiate " + instanceType.getSimpleName());
		}
		
		return instance;
	}

}
