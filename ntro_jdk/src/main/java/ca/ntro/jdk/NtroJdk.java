package ca.ntro.jdk;

import ca.ntro.core.Ntro;

public class NtroJdk extends Ntro {

	public static void initialize() {
		
		Ntro.factory = new FactoryJdk();
	}

}
