package ca.ntro.test.jdk;

import ca.ntro.core.Ntro;
import ca.ntro.jdk.NtroJdk;

public class TestJdk {
	
	static {
		
		NtroJdk.initialize();
	}
	
	
	public static void main(String[] args) {
		
		Ntro.log("TestJdk");
	}

}
