package ca.ntro.demo.fxonly;

import java.io.IOException;
import java.net.URL;

import ca.ntro.core.Ntro;
import ca.ntro.fx.NtroFx;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class DemoFxOnly extends Application {
	
	static {

		NtroFx.initialize();

	}

	private final double SPEED = 200;
	private double offsetx = 0;
	private double offsety = 0;
	private double speedx = SPEED;
	private double speedy = SPEED;
	private final double WORLD_WIDTH  = 640;
	private final double WORLD_HEIGHT = 320;

    public void runDemo(RootViewFxOnly rootView){

        Ntro.log("Animating on canvas");
        
        Ntro.clock().onEveryTick(secondsElapsed -> {
        	
        	offsetx += secondsElapsed * speedx;
        	offsety += secondsElapsed * speedy;
        	
        	if(offsetx + 50 >= WORLD_WIDTH) {

        		speedx = -SPEED;
        		offsetx = WORLD_WIDTH - 50;

        	}else if(offsetx <= 0) {

        		speedx = SPEED;
        		offsetx = 0;
        	}

        	if(offsety + 100 >= WORLD_HEIGHT) {

        		speedy = -SPEED;
        		offsety = WORLD_HEIGHT - 100;

        	}else if(offsety <= 0) {

        		speedy = SPEED;
        		offsety = 0;
        	}

			rootView.canvas().draw(gc -> {
				
				gc.getGcFx().setFill(Color.BLUE);

				gc.fillRect(offsetx, offsety, 50, 100);

			});
        });
    }

	
	public static void main(String[] args) {
		Application.launch(args);
	}

    private static URL urlFromPath(String path) {
        URL url = DemoFxOnly.class.getResource(path);

        if(url == null) {
            throw new RuntimeException("Not found " + path);
        }

        return url;
    }


	@Override
	public void start(Stage primaryStage) throws Exception {
		FXMLLoader loader = new FXMLLoader(urlFromPath("/root.xml"));

		try {

			Parent parent = loader.load();
			RootViewFxOnly rootView = loader.getController();
			
			URL cssUrl = urlFromPath("/prod.css");
			parent.getStylesheets().add(cssUrl.toExternalForm());
			
			primaryStage.setWidth(640);
			primaryStage.setHeight(320);
			primaryStage.setScene(new Scene(parent, 640, 320));
			primaryStage.show();
			
			runDemo(rootView);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
