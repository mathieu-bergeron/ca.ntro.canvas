package ca.ntro.demo.fxonly;

import java.net.URL;
import java.util.ResourceBundle;

import ca.ntro.core.NtroCanvas;
import ca.ntro.fx.NtroCanvasFx;
import ca.ntro.fx.NtroGraphicsContextFx;
import ca.ntro.fx.ViewFx;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;

public class RootViewFxOnly extends ViewFx {
	
	@FXML
	private Canvas canvas;

	private NtroCanvas<NtroGraphicsContextFx> ntroCanvas;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		
		ntroCanvas = NtroCanvasFx.newInstance(canvas);
	}

	public NtroCanvas<NtroGraphicsContextFx> canvas() {
		return ntroCanvas;
	}

}
