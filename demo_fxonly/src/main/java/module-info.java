module ca.ntro.demo.fx {

    requires ca.ntro.core;
    requires ca.ntro.jdk;
    requires ca.ntro.fx;

    requires javafx.base;
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires javafx.media;
    
    exports ca.ntro.demo.fxonly;
    
    opens ca.ntro.demo.fxonly to javafx.fxml;

}
