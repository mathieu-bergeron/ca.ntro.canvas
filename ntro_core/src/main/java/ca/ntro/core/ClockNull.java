package ca.ntro.core;

public class ClockNull implements Clock {

	@Override
	public void onEveryTick(TickLambda lambda) {
		throw new RuntimeException("[FATAL] Ntro.clock() must be initialized");
	}

}
