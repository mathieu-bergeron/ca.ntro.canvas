package ca.ntro.core;

public interface Factory {
	
	// JSweet: cannot use <I, T extends I> (will not compile)
	<I, T> T newInstance(Class<I> instanceType);
	
	<T> void registerSpecialization(Class<? extends T> instanceType, Class<? extends T> specializationType);

	<T> void requiresFurtherSpecialization(Class<? extends T> instanceType);

}
