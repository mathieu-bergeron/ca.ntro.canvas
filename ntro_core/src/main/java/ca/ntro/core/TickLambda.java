package ca.ntro.core;

public interface TickLambda {
	
	void onNextTick(double secondsElapsed);

}
