package ca.ntro.core;

public class Ntro {
	
	protected static Logger logger = new LoggerCore();

	public static void log(String message) {
		logger.log(message);
	}
	
	protected static Clock clock = new ClockNull();

	public static Clock clock() {
		return clock;
	}
	
	protected static Factory factory = new FactoryNull();

	public static Factory factory() {
		return factory;
	}
}
