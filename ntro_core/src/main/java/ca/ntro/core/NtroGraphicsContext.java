package ca.ntro.core;

public interface NtroGraphicsContext {
	
	void fillRect(double topLeftX, double topLeftY, double width, double height);

}
