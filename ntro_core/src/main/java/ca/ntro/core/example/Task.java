package ca.ntro.core.example;

import ca.ntro.core.Ntro;

public interface Task {
	
	public static Task newInstance() {
		Task instance = Ntro.factory().newInstance(Task.class);
		return instance;
	}

	void isPreconditionOf(Task otherTask);
}
