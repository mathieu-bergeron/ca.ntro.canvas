package ca.ntro.core.example;

public interface TaskImpl extends Task {
	
	// XXX: add methods that we do not 
	//      want to expose in the API
	TaskImpl nextTask(); 

}
