package ca.ntro.core;

public interface Logger {

    void log(String message);
}
