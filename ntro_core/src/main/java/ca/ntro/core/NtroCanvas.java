package ca.ntro.core;

public interface NtroCanvas<GC extends NtroGraphicsContext> {
	
	void draw(NtroCanvasDrawLambda<GC> lambda);

}
