package ca.ntro.core;

public interface NtroCanvasDrawLambda<GC extends NtroGraphicsContext> {
	
	void draw(GC gc);

}
