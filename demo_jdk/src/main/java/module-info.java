module ca.ntro.demo.jdk {

    requires ca.ntro.core;
    requires ca.ntro.jdk;

    requires ca.ntro.demo.core;
    
    // XXX: uncomment after adding a first class in ca.ntro.demo.jdk
    //exports ca.ntro.demo.jdk;
}
