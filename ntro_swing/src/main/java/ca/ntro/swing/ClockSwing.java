package ca.ntro.swing;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Timer;

import ca.ntro.core.Clock;
import ca.ntro.core.TickLambda;

public class ClockSwing implements Clock {

	@Override
	public void onEveryTick(TickLambda lambda) {
		Timer timer = new Timer(1, new ActionListener() {

			private long lastTick = System.nanoTime();

			@Override
			public void actionPerformed(ActionEvent arg0) {
				long now = System.nanoTime();

				double secondsElapsed = (now -lastTick) / 1E9;
				lambda.onNextTick(secondsElapsed);

				lastTick = now;
			}
		});
		
		timer.start();
	}

}
