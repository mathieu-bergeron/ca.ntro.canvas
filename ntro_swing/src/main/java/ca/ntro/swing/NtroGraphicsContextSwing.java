package ca.ntro.swing;

import java.awt.Graphics;

import ca.ntro.core.Ntro;
import ca.ntro.core.NtroGraphicsContext;

public class NtroGraphicsContextSwing implements NtroGraphicsContext {

	/* <Factory> */
	
	public static NtroGraphicsContextSwing newInstance(Graphics gcSwing) {

		NtroGraphicsContextSwing instance = Ntro.factory().newInstance(NtroGraphicsContextSwing.class);

		instance.setGcSwing(gcSwing);

		return instance;
	}

	/* </Factory> */
	
	
	
	
	/* <Attributes> */

	
	private Graphics gcSwing;

	public Graphics getGcSwing() {
		return gcSwing;
	}

	public void setGcSwing(Graphics gcSwing) {
		this.gcSwing = gcSwing;
	}

	/* </Attributes> */
	
	
	

	@Override
	public void fillRect(double topLeftX, double topLeftY, double width, double height) {
		gcSwing.fillRect((int) topLeftX, 
				    (int) topLeftY, 
				    (int) width,
				    (int) height);
		
	}

}
