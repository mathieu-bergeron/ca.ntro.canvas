package ca.ntro.swing;

import ca.ntro.core.Ntro;
import ca.ntro.core.NtroCanvas;
import ca.ntro.core.NtroCanvasDrawLambda;

public class NtroCanvasSwing implements NtroCanvas<NtroGraphicsContextSwing> {
	
	/* <Factory> */

	public static NtroCanvasSwing newInstance(NtroCanvasSwingComponent canvas) {
		
		NtroCanvasSwing instance = Ntro.factory().newInstance(NtroCanvasSwing.class);
		
		instance.setCanvas(canvas);
		
		return instance;
	}
	
	/* </Factory> */
	
	
	/* <Atributes> */

	
	private NtroCanvasSwingComponent canvas;

	public NtroCanvasSwingComponent getCanvas() {
		return canvas;
	}

	public void setCanvas(NtroCanvasSwingComponent canvas) {
		this.canvas = canvas;
	}

	/* </Atributes> */

	
	
	@Override
	public void draw(NtroCanvasDrawLambda<NtroGraphicsContextSwing> lambda) {
		canvas.drawWhenReady(lambda);
	}
}
