package ca.ntro.swing;

import java.awt.Graphics;

import javax.swing.JComponent;

import ca.ntro.core.NtroCanvasDrawLambda;
import ca.ntro.jdk.NtroDelayedDrawing;

public class NtroCanvasSwingComponent extends JComponent {
	
	private NtroDelayedDrawing delayedDrawing = new NtroDelayedDrawing();

	@Override
	public void paint(Graphics gcSwing) {
		delayedDrawing.drawNow(NtroGraphicsContextSwing.newInstance(gcSwing));
	}

	public void drawWhenReady(NtroCanvasDrawLambda lambda) {
		delayedDrawing.drawWhenReady(lambda);
		repaint();
	}
}
