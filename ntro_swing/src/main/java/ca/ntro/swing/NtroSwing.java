package ca.ntro.swing;

import ca.ntro.core.Ntro;
import ca.ntro.jdk.NtroJdk;

public class NtroSwing extends NtroJdk {

	public static void initialize() {
		
		NtroJdk.initialize();
		
		Ntro.clock = new ClockSwing();
		
	}

}
