package ca.ntro.test.swing;

import ca.ntro.core.Ntro;
import ca.ntro.swing.NtroSwing;

public class TestSwing {
	
	static {
		
		NtroSwing.initialize();
	}
	

	public static void main(String[] args) {
		
		Ntro.log("TestSwing");
	}

}
