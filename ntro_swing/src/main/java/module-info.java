module ca.ntro.swing {

    requires ca.ntro.core;
    requires ca.ntro.jdk;
    
    requires java.desktop;
    
    exports ca.ntro.swing;

}
