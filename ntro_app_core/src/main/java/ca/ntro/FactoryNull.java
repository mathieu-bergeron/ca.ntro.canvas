package ca.ntro;

public class FactoryNull implements Factory {
	
	private void fatal() {
		throw new RuntimeException("[FATAL] Factory must be initialized");
	}

	@Override
	public <I, T> T newInstance(Class<I> instanceType) {
		fatal();
		return null;
	}

	@Override
	public <T> void registerSpecialization(Class<? extends T> instanceType, Class<? extends T> specializationType) {
		fatal();
	}

	@Override
	public <T> void requiresFurtherSpecialization(Class<? extends T> instanceType) {
		fatal();
	}



}
