package ca.ntro;

public interface TickLambda {
	
	void onNextTick(double secondsElapsed);

}
