package ca.ntro;

public interface NtroCanvas<GC extends NtroGraphicsContext> {
	
	void draw(NtroCanvasDrawLambda<GC> lambda);

}
