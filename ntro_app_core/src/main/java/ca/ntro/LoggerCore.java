package ca.ntro;

public class LoggerCore implements Logger {

    @Override
    public void log(String message) {
        System.out.println("[LoggerCore] " + message);
    }

}
