package ca.ntro;

import java.util.HashMap;
import java.util.Map;

public abstract class FactoryCore implements Factory {
	
	private Map<Class<?>, Class<?>> specializations = new HashMap<>();

	@Override
	public <I, T> T newInstance(Class<I> instanceType) {
		
		Class<T> implementationType = findImplementationType(instanceType);
		
		return buildNewInstance(implementationType);
	}
	
	protected abstract <T> T buildNewInstance(Class<? extends T> instanceType);
	
	public <I, T> Class<T> findImplementationType(Class<I> instanceType) {

		Class<T> implementationType = null;

		Class<T> specializationType = (Class<T>) instanceType;

		do {

			implementationType = specializationType;

			specializationType = (Class<T>) specializations.get(implementationType);
			
			
		}while(specializationType != null);
		
		return implementationType;
	}

	@Override
	public <T> void registerSpecialization(Class<? extends T> instanceType, Class<? extends T> specializationType) {
		specializations.put(instanceType, specializationType);
	}

	@Override
	public <T> void requiresFurtherSpecialization(Class<? extends T> instanceType) {
		// TODO Auto-generated method stub
		
	}

}
