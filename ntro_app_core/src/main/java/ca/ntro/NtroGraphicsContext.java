package ca.ntro;

public interface NtroGraphicsContext {
	
	void fillRect(double topLeftX, double topLeftY, double width, double height);

}
