package ca.ntro.demo.core;

import ca.ntro.core.NtroCanvas;

public interface RootView {
	
	NtroCanvas<DemoGraphicsContext> canvas();

}
