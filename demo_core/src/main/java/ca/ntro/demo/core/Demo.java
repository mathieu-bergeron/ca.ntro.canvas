package ca.ntro.demo.core;

import ca.ntro.core.Ntro;
import ca.ntro.core.NtroGraphicsContext;

public class Demo {
	
	public static void initialize() {
		// TODO 93671b2
		Ntro.factory().registerSpecialization(NtroGraphicsContext.class, DemoGraphicsContext.class);

		Ntro.factory().requiresFurtherSpecialization(DemoGraphicsContext.class);
	}
	
	private final double SPEED = 200;
	private double offsetx = 0;
	private double offsety = 0;
	private double speedx = SPEED;
	private double speedy = SPEED;
	private final double WORLD_WIDTH  = 640;
	private final double WORLD_HEIGHT = 320;

    public void runDemo(RootView rootView){

        Ntro.log("Animating on canvas");
        
        Ntro.clock().onEveryTick(secondsElapsed -> {
        	
        	offsetx += secondsElapsed * speedx;
        	offsety += secondsElapsed * speedy;
        	
        	if(offsetx + 50 >= WORLD_WIDTH) {

        		speedx = -SPEED;
        		offsetx = WORLD_WIDTH - 50;

        	}else if(offsetx <= 0) {

        		speedx = SPEED;
        		offsetx = 0;
        	}

        	if(offsety + 100 >= WORLD_HEIGHT) {

        		speedy = -SPEED;
        		offsety = WORLD_HEIGHT - 100;

        	}else if(offsety <= 0) {

        		speedy = SPEED;
        		offsety = 0;
        	}

			rootView.canvas().draw(gc -> {

				gc.setFill("blue");

				gc.fillRect(offsetx, offsety, 50, 100);

			});
        });
    }
}
