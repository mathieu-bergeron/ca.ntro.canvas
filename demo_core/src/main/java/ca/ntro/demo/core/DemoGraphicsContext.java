package ca.ntro.demo.core;

import ca.ntro.core.NtroGraphicsContext;

public interface DemoGraphicsContext extends NtroGraphicsContext {
	
	void setFill(String colorName);

}
