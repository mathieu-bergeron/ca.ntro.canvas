package ca.ntro.jsweet;

import def.dom.CanvasRenderingContext2D;
import def.dom.HTMLCanvasElement;

import static jsweet.util.StringTypes._2d;

import ca.ntro.core.Ntro;
import ca.ntro.core.NtroCanvas;
import ca.ntro.core.NtroCanvasDrawLambda;

public class NtroCanvasJSweet implements NtroCanvas<NtroGraphicsContextJSweet> {
	
	/* <Factory> */
	
	
	public static NtroCanvasJSweet newInstance(HTMLCanvasElement canvas) {

		NtroCanvasJSweet          instance   = Ntro.factory().newInstance(NtroCanvasJSweet.class);
		CanvasRenderingContext2D  gcJSweet   = canvas.getContext(_2d);
		NtroGraphicsContextJSweet gc         = NtroGraphicsContextJSweet.newInstance(gcJSweet);

		instance.setCanvas(canvas);
		instance.setGcJSweet(gcJSweet);
		instance.setGc(gc);

		return instance;
	}
	
	public NtroCanvasJSweet() {
		super();
	}

	/* </Factory> */
	
	
	
	

	/* <Attributes> */

	private HTMLCanvasElement canvas;
	private CanvasRenderingContext2D gcJSweet;
	private NtroGraphicsContextJSweet gc;

	public HTMLCanvasElement getCanvas() {
		return canvas;
	}

	public void setCanvas(HTMLCanvasElement canvas) {
		this.canvas = canvas;
	}


	public CanvasRenderingContext2D getGcJSweet() {
		return gcJSweet;
	}


	public void setGcJSweet(CanvasRenderingContext2D gcJSweet) {
		this.gcJSweet = gcJSweet;
	}


	public NtroGraphicsContextJSweet getGc() {
		return gc;
	}


	public void setGc(NtroGraphicsContextJSweet gc) {
		this.gc = gc;
	}

	/* </Attributes> */

	
	
	
	
	

	@Override
	public void draw(NtroCanvasDrawLambda lambda) {
		gcJSweet.clearRect(0, 0, canvas.width, canvas.height);
		lambda.draw(gc);
	}

}
