package ca.ntro.jsweet;

import ca.ntro.core.Ntro;

public class NtroJSweet extends Ntro {

    public static void initialize() {
    	
    	Ntro.factory = new FactoryJSweet();

    	Ntro.logger = new LoggerJSweet();
    	Ntro.clock = new ClockJSweet();
    }

}
