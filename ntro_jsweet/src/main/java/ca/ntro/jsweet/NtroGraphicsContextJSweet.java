package ca.ntro.jsweet;

import ca.ntro.core.Ntro;
import ca.ntro.core.NtroGraphicsContext;
import def.dom.CanvasRenderingContext2D;


public class NtroGraphicsContextJSweet implements NtroGraphicsContext {

	/* <Factory> */
	
	public static NtroGraphicsContextJSweet newInstance(CanvasRenderingContext2D gcJSweet) {

		NtroGraphicsContextJSweet instance = Ntro.factory().newInstance(NtroGraphicsContextJSweet.class);

		instance.setGcJSweet(gcJSweet);

		return instance;
	}

	/* </Factory> */
	
	
	
	
	/* <Attributes> */

	private CanvasRenderingContext2D gcJSweet; 

	public CanvasRenderingContext2D getGcJSweet() {
		return gcJSweet;
	}

	public void setGcJSweet(CanvasRenderingContext2D gcJSweet) {
		this.gcJSweet = gcJSweet;
	}

	/* </Attributes> */
	
	
	
	

	@Override
	public void fillRect(double topLeftX, double topLeftY, double width, double height) {
		gcJSweet.fillRect(topLeftX, topLeftY, width, height);
	}

}
