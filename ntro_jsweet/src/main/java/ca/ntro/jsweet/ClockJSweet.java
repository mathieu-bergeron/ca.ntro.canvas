package ca.ntro.jsweet;

import static def.dom.Globals.window;
import static jsweet.util.Lang.function;

import ca.ntro.core.Clock;
import ca.ntro.core.TickLambda;

public class ClockJSweet implements Clock {
	
	private long lastTick = System.nanoTime();

	@Override
	public void onEveryTick(TickLambda lambda) {
		
		window.setInterval(function(() -> {
			
			long now = System.nanoTime();
			double elapsed = (now - lastTick) / 1E9;
			
			lastTick = now;

			lambda.onNextTick(elapsed);
			
		}), 10, new Object());
	}


}
