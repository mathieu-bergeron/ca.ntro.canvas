package ca.ntro.jsweet;

import ca.ntro.core.FactoryCore;

public class FactoryJSweet extends FactoryCore {
	
	@Override
	protected <T> T buildNewInstance(Class<? extends T> instanceType) {
		T instance = null;
		
		try {

			instance = instanceType.newInstance();

		} catch (InstantiationException | IllegalAccessException e) {

			throw new RuntimeException("[FATAL] cannot instantiate " + instanceType.getSimpleName());
		}
		
		return instance;
	}

}
