package ca.ntro.jsweet;

import ca.ntro.core.Logger;

public class LoggerJSweet implements Logger {

	@Override
	public void log(String message) {
		System.err.println("[LoggerJSweet] " + message);
	}

}
