module ca.ntro.jsweet {

    requires ca.ntro.core;
	requires jsweet.core;

    exports ca.ntro.jsweet;

}
