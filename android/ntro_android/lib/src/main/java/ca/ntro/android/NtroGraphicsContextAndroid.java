package ca.ntro.android;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import ca.ntro.core.Ntro;
import ca.ntro.core.NtroGraphicsContext;

public class NtroGraphicsContextAndroid implements NtroGraphicsContext {

    /* <Factory> */
    public static NtroGraphicsContextAndroid newInstance(Canvas gcAndroid){

        NtroGraphicsContextAndroid instance = Ntro.factory().newInstance(NtroGraphicsContextAndroid.class);

        instance.setGcAndroid(gcAndroid);

        instance.initialize();

        return instance;
    }

    public void initialize(){
        setCurrentPaint(new Paint(Color.BLACK));
    }

    /* </Factory> */


    /* <Attributes> */

    private Canvas gcAndroid;
    private Paint currentPaint;

    public Canvas getGcAndroid() {
        return gcAndroid;
    }

    public void setGcAndroid(Canvas gcAndroid) {
        this.gcAndroid = gcAndroid;
    }

    public Paint getCurrentPaint() {
        return currentPaint;
    }

    public void setCurrentPaint(Paint currentPaint) {
        this.currentPaint = currentPaint;
    }

    /* <Attributes> */





    @Override
    public void fillRect(double topLeftX, double topLeftY, double width, double height) {
        getGcAndroid().drawRect((float) topLeftX,
                                (float) topLeftY,
                                (float) (topLeftX + width),
                                (float) (topLeftY + height),
                                currentPaint);
    }
}
