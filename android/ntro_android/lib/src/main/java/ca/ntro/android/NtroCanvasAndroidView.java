package ca.ntro.android;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import ca.ntro.core.NtroCanvasDrawLambda;
import ca.ntro.jdk.NtroDelayedDrawing;

public class NtroCanvasAndroidView extends View {

    private NtroDelayedDrawing delayedDrawing = new NtroDelayedDrawing();

    public NtroCanvasAndroidView(Context context) {
        super(context);
    }

    public NtroCanvasAndroidView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public NtroCanvasAndroidView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public NtroCanvasAndroidView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void onDraw(Canvas canvas){
        super.onDraw(canvas);

        delayedDrawing.drawNow(NtroGraphicsContextAndroid.newInstance(canvas));
    }

    public void drawWhenReady(NtroCanvasDrawLambda lambda) {
        delayedDrawing.drawWhenReady(lambda);
        invalidate();
    }
}
