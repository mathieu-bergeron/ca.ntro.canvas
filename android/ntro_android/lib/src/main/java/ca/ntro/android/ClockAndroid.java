package ca.ntro.android;

import android.animation.TimeAnimator;

import ca.ntro.core.Clock;
import ca.ntro.core.TickLambda;

public class ClockAndroid implements Clock {

    @Override
    public void onEveryTick(TickLambda lambda) {
        TimeAnimator animator = new TimeAnimator();

        animator.setTimeListener(new TimeAnimator.TimeListener() {
            @Override
            public void onTimeUpdate(TimeAnimator timeAnimator, long l, long deltaTime) {
                double elapsedTime = deltaTime / 1E3;

                lambda.onNextTick(elapsedTime);
            }
        });

        animator.start();
    }
}
