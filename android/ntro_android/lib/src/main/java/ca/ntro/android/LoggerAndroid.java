package ca.ntro.android;

import android.util.Log;

import ca.ntro.core.Logger;

public class LoggerAndroid implements Logger {

    @Override
    public void log(String message) {
        Log.d("LoggerAndroid", message);

    }
}
