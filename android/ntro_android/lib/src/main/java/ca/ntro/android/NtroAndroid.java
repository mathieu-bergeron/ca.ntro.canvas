package ca.ntro.android;

import ca.ntro.core.Ntro;
import ca.ntro.jdk.NtroJdk;

public class NtroAndroid extends NtroJdk {

    public static void initialize(){

        NtroJdk.initialize();

        Ntro.logger = new LoggerAndroid();
        Ntro.clock = new ClockAndroid();
    }
}
