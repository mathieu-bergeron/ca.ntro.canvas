package ca.ntro.android;

import ca.ntro.core.NtroCanvas;
import ca.ntro.core.NtroCanvasDrawLambda;

public class NtroCanvasAndroid implements NtroCanvas {

    NtroCanvasAndroidView canvas;

    public NtroCanvasAndroid(NtroCanvasAndroidView canvas){
        this.canvas = canvas;
    }

    @Override
    public void draw(NtroCanvasDrawLambda lambda) {
        canvas.drawWhenReady(lambda);
    }

}
