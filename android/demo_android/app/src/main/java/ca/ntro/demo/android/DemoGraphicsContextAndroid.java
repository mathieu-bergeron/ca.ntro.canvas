package ca.ntro.demo.android;

import android.graphics.Color;
import android.graphics.Paint;

import ca.ntro.android.NtroGraphicsContextAndroid;
import ca.ntro.demo.core.DemoGraphicsContext;

public class DemoGraphicsContextAndroid extends NtroGraphicsContextAndroid implements DemoGraphicsContext {

    @Override
    public void setFill(String colorName) {
        Paint currentPaint = new Paint(Color.RED);

        if(colorName.equalsIgnoreCase("blue")){

            currentPaint = new Paint(Color.BLUE);
        }

        setCurrentPaint(currentPaint);
    }
}
