package ca.ntro.demo.android;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import ca.ntro.android.NtroAndroid;
import ca.ntro.android.NtroCanvasAndroid;
import ca.ntro.android.NtroCanvasAndroidView;
import ca.ntro.android.NtroGraphicsContextAndroid;
import ca.ntro.core.Ntro;
import ca.ntro.core.NtroGraphicsContext;
import ca.ntro.demo.core.Demo;

public class MainActivity extends AppCompatActivity {

    private Demo demo = new Demo();

    static{

        NtroAndroid.initialize();

        Ntro.factory().registerSpecialization(NtroGraphicsContextAndroid.class, DemoGraphicsContextAndroid.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NtroCanvasAndroidView canvasView = findViewById(R.id.ntroCanvas);

        RootViewAndroid rootView = new RootViewAndroid(canvasView);

        demo.runDemo(rootView);
    }
}