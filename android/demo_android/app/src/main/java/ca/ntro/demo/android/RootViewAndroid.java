package ca.ntro.demo.android;

import ca.ntro.android.NtroCanvasAndroid;
import ca.ntro.android.NtroCanvasAndroidView;
import ca.ntro.core.NtroCanvas;
import ca.ntro.demo.core.RootView;

public class RootViewAndroid implements RootView {

    private NtroCanvasAndroid canvas;

    public RootViewAndroid(NtroCanvasAndroidView canvas){
        this.canvas = new NtroCanvasAndroid(canvas);
    }

    @Override
    public NtroCanvas canvas() {
        return canvas;
    }
}
