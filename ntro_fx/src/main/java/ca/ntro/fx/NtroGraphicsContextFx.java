package ca.ntro.fx;

import ca.ntro.core.Ntro;
import ca.ntro.core.NtroGraphicsContext;
import javafx.scene.canvas.GraphicsContext;

public class NtroGraphicsContextFx implements NtroGraphicsContext {
	
	/* <Factory> */
	
	public static NtroGraphicsContextFx newInstance(GraphicsContext gcFx) {

		NtroGraphicsContextFx instance = Ntro.factory().newInstance(NtroGraphicsContext.class);

		instance.setGcFx(gcFx);

		return instance;
	}

	/* </Factory> */
	
	
	
	
	
	/* <Attributes> */

	private GraphicsContext gcFx;

	public GraphicsContext getGcFx() {
		return gcFx;
	}

	public void setGcFx(GraphicsContext gc) {
		this.gcFx = gc;
	}

	/* </Attributes> */
	
	
	
	
	
	@Override
	public void fillRect(double topLeftX, double topLeftY, double width, double height) {
		getGcFx().fillRect(topLeftX, topLeftY, width, height);
	}
}
