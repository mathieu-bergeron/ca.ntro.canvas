package ca.ntro.fx;

import ca.ntro.core.Ntro;
import ca.ntro.core.NtroCanvas;
import ca.ntro.core.NtroCanvasDrawLambda;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;

public class NtroCanvasFx implements NtroCanvas {

	/* <Factory> */

	public static NtroCanvasFx newInstance(Canvas canvas) {

		NtroCanvasFx instance    = Ntro.factory().newInstance(NtroCanvasFx.class);
		GraphicsContext gcFx     = canvas.getGraphicsContext2D();
		NtroGraphicsContextFx gc = NtroGraphicsContextFx.newInstance(gcFx);

		instance.setCanvas(canvas);
		instance.setGcFx(gcFx);
		instance.setGc(gc);

		return instance;
	}

	/* </Factory> */
	
	
	
	/* </Attributes> */

	private Canvas canvas;
	private GraphicsContext gcFx;
	private NtroGraphicsContextFx gc;

	public Canvas getCanvas() {
		return canvas;
	}

	public void setCanvas(Canvas canvas) {
		this.canvas = canvas;
	}

	public GraphicsContext getGcFx() {
		return gcFx;
	}

	public void setGcFx(GraphicsContext gcFx) {
		this.gcFx = gcFx;
	}

	public NtroGraphicsContextFx getGc() {
		return gc;
	}

	public void setGc(NtroGraphicsContextFx gc) {
		this.gc = gc;
	}

	/* </Attributes> */

	
	
	
	
	
	@Override
	public void draw(NtroCanvasDrawLambda lambda) {
		gcFx.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
		lambda.draw(gc);
	}

}
