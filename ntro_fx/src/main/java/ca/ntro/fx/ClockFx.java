package ca.ntro.fx;

import ca.ntro.core.Clock;
import ca.ntro.core.TickLambda;
import javafx.animation.AnimationTimer;

public class ClockFx implements Clock {
	
	@Override
	public void onEveryTick(TickLambda lambda) {
		new AnimationTimer() {
			
			private long lastTick = System.nanoTime();

			@Override
			public void handle(long now) {
				double secondsElapsed = (now -lastTick) / 1E9;
				lambda.onNextTick(secondsElapsed);

				lastTick = now;
			}
			
		}.start();
	}

}
