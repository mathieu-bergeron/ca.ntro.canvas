package ca.ntro.fx;


import ca.ntro.core.Ntro;
import ca.ntro.core.NtroGraphicsContext;
import ca.ntro.core.example.Task;
import ca.ntro.core.example.TaskCore;
import ca.ntro.jdk.NtroJdk;

public class NtroFx extends NtroJdk {

	public static void initialize() {
		
		NtroJdk.initialize();

		Ntro.clock = new ClockFx();
		
		Ntro.factory().registerSpecialization(NtroGraphicsContext.class, NtroGraphicsContextFx.class);
		
		Ntro.factory().registerSpecialization(Task.class, TaskCore.class);
	}

}
