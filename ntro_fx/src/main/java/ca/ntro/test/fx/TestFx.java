package ca.ntro.test.fx;

import ca.ntro.core.Ntro;
import ca.ntro.fx.NtroFx;

public class TestFx {
	
	static {
		
		NtroFx.initialize();
	}
	
	
	public static void main(String[] args) {
		
		Ntro.log("TestFx");
	}

}
