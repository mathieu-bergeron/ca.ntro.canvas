module ca.ntro.fx {

    requires ca.ntro.core;
    requires ca.ntro.jdk;
	requires javafx.fxml;
	requires javafx.graphics;
    
    exports ca.ntro.fx;


}
