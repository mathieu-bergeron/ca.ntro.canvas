package ca.ntro.demo.jsweet;

import ca.ntro.core.NtroCanvas;
import ca.ntro.demo.core.RootView;
import ca.ntro.jsweet.NtroCanvasJSweet;
import def.dom.HTMLCanvasElement;

public class RootViewJSweet implements RootView {

	private NtroCanvasJSweet canvas;
	
	public RootViewJSweet(HTMLCanvasElement canvas) {

		this.canvas = NtroCanvasJSweet.newInstance(canvas);
	}

	@Override
	public NtroCanvas canvas() {
		return canvas;
	}

}
