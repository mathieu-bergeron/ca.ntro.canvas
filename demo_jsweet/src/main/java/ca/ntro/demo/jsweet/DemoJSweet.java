package ca.ntro.demo.jsweet;

import ca.ntro.core.Ntro;
import ca.ntro.demo.core.Demo;
import ca.ntro.jsweet.NtroGraphicsContextJSweet;
import ca.ntro.jsweet.NtroJSweet;
import def.dom.Event;
import def.dom.HTMLCanvasElement;

import static def.dom.Globals.document;
import static def.dom.Globals.window;

import java.util.function.Function;

public class DemoJSweet extends Demo {

	static {

		NtroJSweet.initialize();
		
		Ntro.factory().registerSpecialization(NtroGraphicsContextJSweet.class, DemoGraphicsContextJSweet.class);
	}

    public static void main(String[] args){
    	
    	
    	window.onload = new Function<Event, Object>() {

			@Override
			public Object apply(Event t) {

				HTMLCanvasElement canvas = (HTMLCanvasElement) document.getElementsByTagName("canvas").$get(0);
				
				RootViewJSweet rootView = new RootViewJSweet(canvas);

				DemoJSweet demoJSweet = new DemoJSweet();
				demoJSweet.runDemo(rootView);
				
				return null;
			}
		};


    }
}
