package ca.ntro.demo.jsweet;

import ca.ntro.demo.core.DemoGraphicsContext;

import ca.ntro.jsweet.NtroGraphicsContextJSweet;

import static jsweet.util.Lang.union;

public class DemoGraphicsContextJSweet extends NtroGraphicsContextJSweet implements DemoGraphicsContext {

	@Override
	public void setFill(String colorString) {
		getGcJSweet().fillStyle = union(colorString);
	}

}
