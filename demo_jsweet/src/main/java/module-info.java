module ca.ntro.demo.jsweet {

    requires ca.ntro.core;
    requires ca.ntro.jsweet;

    requires ca.ntro.demo.core;
	requires jsweet.core;

}
