# Prototype Ntro pour un World2dCanvas en Fx, Swing, JSweet et Android

## Dépendances

1. JDK 15 (la plus haute version supportée par JSweet en ce moment)

1. `nodejs` et `npm` (JSweet se sert de `npm` pour installer `tsc`)

## Créer les projets Eclipse

```bash
$ sh gradlew eclipse
```

## Exécuter pour Fx et Swing

```bash
$ sh gradlew demo_fx
```

```bash
$ sh gradlew demo_swing
```

## Exécuter pour JSweet

1. Compiler en Javascript

```bash
$ sh gradlew demo_jsweet
```

1. Ouvrir la page

```bash
$ firefox demo_jsweet/index.html
```

1. Ouvrir la console pour voir le message 

## Exécuter pour Android

1. Ouvrir le projet `android/demo_android` dans AndroidStudio

1. Synchroniser Gradle

1. Exécuter l'application à partir de AndroidStudio
