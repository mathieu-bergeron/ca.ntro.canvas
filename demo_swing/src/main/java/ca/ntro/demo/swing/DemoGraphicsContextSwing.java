package ca.ntro.demo.swing;

import java.awt.Color;

import ca.ntro.demo.core.DemoGraphicsContext;
import ca.ntro.swing.NtroGraphicsContextSwing;

public class DemoGraphicsContextSwing extends NtroGraphicsContextSwing implements DemoGraphicsContext {

	@Override
	public void setFill(String colorName) {

		if(colorName.equalsIgnoreCase("blue")) {

			getGcSwing().setColor(Color.BLUE);

		}else {

			getGcSwing().setColor(Color.BLACK);
		}
	}

}
