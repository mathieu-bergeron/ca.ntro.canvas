package ca.ntro.demo.swing;


import javax.swing.JFrame;

import ca.ntro.core.Ntro;
import ca.ntro.demo.core.Demo;
import ca.ntro.swing.NtroGraphicsContextSwing;
import ca.ntro.swing.NtroSwing;

public class DemoSwing extends Demo {

	static {

		NtroSwing.initialize();
		
		DemoSwing.initialize();
		
		Ntro.factory().registerSpecialization(NtroGraphicsContextSwing.class, DemoGraphicsContextSwing.class);
	}
	
	public static void main(String[] args) {
		
		JFrame frame = new JFrame("DemoSwing");
		RootViewSwing rootView = new RootViewSwing();
		
		frame.setSize(640, 320);
		
		frame.add(rootView);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.setVisible(true);
		
		
		DemoSwing demoSwing = new DemoSwing();

		demoSwing.runDemo(rootView);
	}


}
