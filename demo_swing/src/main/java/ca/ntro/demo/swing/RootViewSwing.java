package ca.ntro.demo.swing;

import ca.ntro.core.NtroCanvas;
import ca.ntro.demo.core.RootView;
import ca.ntro.swing.NtroCanvasSwing;
import ca.ntro.swing.NtroCanvasSwingComponent;

public class RootViewSwing extends NtroCanvasSwingComponent implements RootView {
	
	private NtroCanvasSwing canvas;

	public RootViewSwing() {

		this.canvas = NtroCanvasSwing.newInstance(this);
	}

	@Override
	public NtroCanvas canvas() {
		return canvas;
	}

}
